package updated;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Iterator;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @author vkachhawa
 *
 */
public class Excel {

	public String filepath;
	public Workbook workbook;

	public Excel(String filepath) {
		this.filepath = filepath;
		try {
			getWorkbook();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void getWorkbook() throws IOException {
		File file = new File(filepath);
		try {
			FileInputStream inputStream = new FileInputStream(file);
			String fileExtensionName = filepath.substring(filepath.indexOf("."));
			// If Excel is in .xlsx format or .xls
			if (fileExtensionName.equalsIgnoreCase(".xlsx"))
				workbook = new XSSFWorkbook(inputStream);
			else
				workbook = new HSSFWorkbook(inputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}

	public String getCellValue(Sheet sheet, int row, int col) throws IOException {
		String cellVal = null;
		try {
			Cell cell = sheet.getRow(row).getCell(col);
			CellType cell_Type = cell.getCellType(); // Get cell type
			switch (cell_Type) {
			case STRING:
				cellVal = cell.getStringCellValue();
				break;
			case NUMERIC:
				cellVal = (int) cell.getNumericCellValue() + "";
				break;
			case BOOLEAN:
				cellVal = cell.getBooleanCellValue() + "";
				break;
			case FORMULA:
				cellVal = cell.getRichStringCellValue().toString();
				break;
			case BLANK:
				cellVal = "";
				break;
			default:
				cellVal = "";
				break;
			}
		} catch (Exception e) {
			System.out.println("Something went wrong. Cell not found.");
		}
		return cellVal;
	}

	public String getCellvalue(Sheet sheet, String row, String column) throws IOException {
		int columnNumber = getColumnNumber(sheet, column);
		int rowNumber = getRowNumber(sheet, row);
		return getCellValue(sheet, rowNumber, columnNumber);
	}

	public String getcellValue(Sheet sheet, String row, int columnNumber) throws IOException {
		int rowNumber = getRowNumber(sheet, row);

		return getCellValue(sheet, rowNumber, columnNumber);
	}

	public String getcellValue(Sheet sheet, int row, String col) throws IOException {
		int columnNumber = getColumnNumber(sheet, col);
		return getCellValue(sheet, row, columnNumber);
	}

	public void setcellValue(String sheetname, int row, int col, String value) throws IOException {
		FileInputStream inputStream = new FileInputStream(filepath);
		Sheet sheet = workbook.getSheet(sheetname);
		Cell cell = sheet.getRow(row).getCell(col);
		cell.setCellValue(value);
		FileOutputStream outputStream = new FileOutputStream(new File(filepath));
		workbook.write(outputStream);
		outputStream.flush();
		outputStream.close();
		inputStream.close();
	}

	public void setcellValue(Sheet sheet, int row, int col, String value) throws IOException {
		String sheetname = sheet.getSheetName();
		setcellValue(sheetname, row, col, value);
	}

	public void setcellValue(Sheet sheet, int row, String col, String value) throws IOException {
		String sheetname = sheet.getSheetName();
		int intcol = getColumnNumber(sheet, col);
		setcellValue(sheetname, row, intcol, value);
	}

	public void setcellValue(Sheet sheet, String row, int col, String value) throws IOException {
		String sheetname = sheet.getSheetName();
		int introw = getRowNumber(sheet, row);
		setcellValue(sheetname, introw, col, value);
	}

	public void setcellValue(Sheet sheet, String row, String col, String value) throws IOException {
		String sheetname = sheet.getSheetName();
		int introw = getRowNumber(sheet, row);
		int intcol = getColumnNumber(sheet, col);
		setcellValue(sheetname, introw, intcol, value);
	}

	/**
	 * New Method. More Refined.
	 * 
	 * @param sheet
	 * @param row
	 * @return returns -1 if value not found
	 */
	public int getColumnNumber(Sheet sheet, String col) {
		Iterator<Row> iterator = sheet.rowIterator();
		while (iterator.hasNext()) {
			Row row = (Row) iterator.next();
			for (int i = 0; i < row.getLastCellNum(); i++) {
				try {
					if (getCellValue(sheet, row.getRowNum(), i).equals(col))
						return i;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return -1;
	}

	/**
	 * New Method. More Refined.
	 * 
	 * @param sheet
	 * @param row
	 * @return returns -1 if value not found
	 */
	public int getRowNumber(Sheet sheet, String row) {
		Row currentRow;
		Iterator<Cell> currentCell;
		Cell cell;
		Iterator<Row> rowiterator = sheet.rowIterator();
		while (rowiterator.hasNext()) {
			currentRow = (Row) rowiterator.next();
			currentCell = currentRow.cellIterator();
			while (currentCell.hasNext()) {
				cell = (Cell) currentCell.next();
				try {
					if (getCellValue(sheet, currentRow.getRowNum(), cell.getColumnIndex()).equals(row))
						return currentRow.getRowNum();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return -1;
	}

	public Sheet getsheet() {
		return workbook.getSheetAt(0);
	}
	public Sheet getSheet(int sheetnumber) {
		return workbook.getSheetAt(sheetnumber);
	}
	public Sheet getSheet(String sheet) {
		return workbook.getSheet(sheet);
	}

	public int getRowCount(Sheet sheet) {
		return sheet.getLastRowNum();
	}

	public int getColumnCount(Sheet sheet) {
		return sheet.getRow(0).getLastCellNum();
	}

	public void closeWorkbook() {
		try {
			workbook.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
